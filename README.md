## Requirements
* python3
* Docker
* Ansible
* Molecule


### Part 1: Molecule (https://molecule.readthedocs.io/en/stable/)
1) Установить Molecule
2) В конфигурации molecule.yml указать в качестве платформы `ubuntu:18.04`, в качестве драйвера `docker`
3) Создать instance ```molecule create```
4) Настроить доступ со своей машины на поднятую по ssh


### Part 2: Ansible roles
1) Создать пустую поль postgres, руками или через `molecule`
2) Написать следующие задачи:
    - Установка postgresql из репозитория (https://www.postgresql.org/download/)
    - Поменять конфигурацию postgresql.conf (https://pgtune.leopard.in.ua/#/)
    - Поменять конфигурацию postgresql.conf pg_hba.conf
    - Создать новую базу данных
    - Создать нового пользователя для этой базы


### Результат
Протестированая и рабочая роль для развернывания postgresql
