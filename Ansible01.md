# Что такое Ansible?
Инстурмент для управления конфигурацией системы, оркестрирования и автоматизации привидения системы в определенное состояние.

# Что можно делать с помощь Ansible?
* Устанавливать пакеты
* Запускать, останавливать и перезапускать сервисы
* Создавать, удалять, редактировать файлы
* ...

# Как происходит взаимодействие между машинами?
* По ssh:
    * На управляющей машине нужен python и ssh
    * На контролируемой нужен ssh

# Установка Ansible
```bash
pip install ansible

ssh-keygen -f ansible
ssh-add ~/.ssh/ansible
ssh-copy-id -f ~/.ssh/ansible $TARGETHOST

ansible -m ping target
```


# Основные понятия


### <a href="https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#intro-inventory">Inventory</a>
``` ini
mail.example.com

[webservers]
one.example.com
foo.example.com
bar.example.com

[dbservers]
one.example.com
two.example.com
three.example.com
```

* Существует 2 группы по умолчанию: ```all``` и ```ungrouped```
    * all - все хосты, которые есть в инвенторе
        * `mail.example.com`
        * `foo.example.com`
        * `bar.example.com`
        * `one.example.com`
        * `two.example.com`
        * `three.example.com`
    * ungrouped - хосты, не имеющие группы
        * `mail.example.com`


### Modules
* Принимают на вход параметры, выполняют определенные действия
* Похожи чем-то на процедуры в языках программирования
* Пример: ping, apt, postgresql_db
* Документация модуля ```ansible-doc ping```
* <a href="https://docs.ansible.com/ansible/latest/modules/modules_by_category.html#modules-by-category">Ansible Module Index</a>

### Tasks
```yml
  tasks:
  - name: ensure apache is at the latest version
    yum:
      name: httpd
      state: latest
  - name: write the apache config file
    template:
      src: /srv/httpd.j2
      dest: /etc/httpd.conf
```
* Условия: <a href="https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html">when</a>
```yml
- name: install httpd (Red Hat)
  yum:
    name: httpd
  when: 'ansible_distribution == "RedHat"'

- name: install apache (Debian)
  apt:
    name: apache2
  when: 'ansible_distribution == "Debian"'
```
* Циклы: <a href="https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html#playbooks-loops">Loops</a>
```yml
- name: add several users
  user:
    name: "{{ item }}"
    state: present
    groups: "wheel"
  loop:
     - testuser1
     - testuser2
```


### <a href="https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html#about-playbooks">Playbooks</a>
```yml
---
- hosts: webservers
  remote_user: root
  tasks:
  - name: ensure apache is at the latest version
    yum:
      name: httpd
      state: latest
  - name: write the apache config file
    template:
      src: /srv/httpd.j2
      dest: /etc/httpd.conf

- hosts: databases
  remote_user: root
  tasks:
  - name: ensure postgresql is at the latest version
    yum:
      name: postgresql
      state: latest
  - name: ensure that postgresql is started
    service:
      name: postgresql
      state: started
```

```yml
- hosts: all

  include: another_playbook.yml

  tasks:
  - include: setup.yml
  - include: configure.yml
```

### <a href="https://docs.ansible.com/ansible/latest/user_guide/playbooks_templating.html">Templates</a>
```bash
$ cat template.j2
db_host = '{{ database_host }}';
db_name = '{{ database_name }}';
db_user = '{{ database_username }}';
db_pass = '{{ database_password }}';
db_port = {{ database_port}};
```

```yml
- template:
    src: path/to/template.j2
    dest: /path/to/result
```

### <a href="https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html#handlers-running-operations-on-change">Handlers</a>
```yml
handlers:
    - name: restart memcached
      service:
        name: memcached
        state: restarted
      listen: "restart web services"
    - name: restart apache
      service:
        name: apache
        state: restarted
      listen: "restart web services"

tasks:
    - name: template configuration file
      template:
        src: template.j2
        dest: /etc/foo.conf
      notify:
        - restart memcached
        - restart apache

    - name: restart everything
      command: echo "this task will restart the web services"
      notify: "restart web services"
```

### <a href="https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable">Variables</a>
```yml
- name: get stat data for file
  stat:
    path: /path/to/file
  register: stat_file

- name: fail if path doesn't exist
  fail:
    msg: "File does not exist"
  when: not stat_file.stat.exists
```

### <a href="https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html">Roles</a>
* Позволяют разделять tasks, variables, handlers в отдельные группы - роли
* <a href="https://galaxy.ansible.com">Ansible galaxy</a>

## Ссылки
* https://willthames.github.io/devops-singapore-2016/
* https://docs.ansible.com/ansible/latest/
* 